let shoppingListObjects = [
    {
        createdAt: "2022-02-13T17:17:21.592",
        updatedAt: "2022-02-13T15:17:31.593",
        id:1,
        title: "To buy in 2 days..",
        category: "Kitchen",
        description: "A ShoppingList for the kitchen",
        itemList: [
            18,
            17,
            16,
            14,
            13,
            12
        ]
    },
    {
        createdAt: "2022-02-12T17:17:21.592",
        updatedAt: "2022-02-12T16:17:31.593",
        id:2,
        title: "Office Setup",
        category: "Office",
        description: "Some Stuff for the office",
        itemList: [
            15,
            1,
            2
        ]
    },
    {
        createdAt: "2022-02-13T17:17:21.592",
        updatedAt: "2022-02-10T17:17:31.593",
        id:3,
        title: "Birthday Present",
        category: "Family",
        description: "The wished smartphone for my daughter",
        itemList: [
            5
        ]
    },
    {
        createdAt: "2022-02-13T17:17:21.592",
        updatedAt: "2022-02-13T17:10:31.593",
        id:4,
        title: "Healthy Food",
        category: "Kitchen",
        description: "Some Healthy food for my grand mother",
        itemList: [
            8,7,18
        ]
    },
    {
        createdAt: "2022-02-13T17:17:21.592",
        updatedAt: "2022-02-13T14:17:31.593",
        id:5,
        title: "Uni stuff",
        category: "University",
        description: "For the exams period while learning in the library",
        itemList: [
            19,15,18,13,14
        ]
    }
];


let items =  [
        {
            id: 1,
            name: "Pavilion 1",
            description: "PC Pavilion 1 with touch screen feature and i7-Processor",
            price: 949.5,
            categories: [
                "electronics",
                "pc",
                "hp"
            ],
            createdAt: "2022-02-13T14:21:12.587",
            updatedAt: "2022-02-13T14:21:12.587"
        },
    {
        id: 2,
        name: "Dell LED 21",
        description: "LED Monitor with touch screen feature and 21' length",
        price: 750.5,
        categories: [
            "electronics",
            "monitors",
            "dell"
        ],
        createdAt: "2022-02-13T14:21:12.616",
        updatedAt: "2022-02-13T14:21:12.616"
    },
    {
        id: 3,
        name: "Samsung LED 18",
        description: "LED Monitor with ultra high resolution and 18' length",
        price: 639.5,
        categories: [
            "electronics",
            "monitors",
            "samsung"
        ],
        createdAt: "2022-02-13T14:21:12.617",
        updatedAt: "2022-02-13T14:21:12.617"
    },
    {
        id: 4,
        name: "Dell Inspiron PC",
        description: "Dell Notebook with 16Gb RAM memory and i5-Processor",
        price: 809.0,
        categories: [
            "electronics",
            "pc",
            "dell"
        ],
        createdAt: "2022-02-13T14:21:12.619",
        updatedAt: "2022-02-13T14:21:12.619"
    },
    {
        id: 5,
        name: "iPhone 12 Pro 256GB",
        description: "Apple Smartphone 12 Pro with 256Gb Storage capacity",
        price: 1029.0,
        categories: [
            "electronics",
            "smartphone",
            "apple"
        ],
        createdAt: "2022-02-13T14:21:12.62",
        updatedAt: "2022-02-13T14:21:12.62"
    },
    {
        id: 6,
        name: "iPhone 11 Pro 128GB",
        description: "Apple Smartphone 11 Pro with 128Gb Storage capacity",
        price: 849.0,
        categories: [
            "electronics",
            "smartphone",
            "apple"
        ],
        createdAt: "2022-02-13T14:21:12.621",
        updatedAt: "2022-02-13T14:21:12.621"
    },
    {
        id: 7,
        name: "Galaxy 21 FE 128GB",
        description: "Samsung Smartphone Galaxy FE 21  with 128Gb Storage capacity",
        price: 749.0,
        categories: [
            "electronics",
            "smartphone",
            "apple"
        ],
        createdAt: "2022-02-13T14:21:12.623",
        updatedAt: "2022-02-13T14:21:12.623"
    },
    {
        id: 8,
        name: "German Apples",
        description: "Fresh apples from Germany",
        price: 1.49,
        categories: [
            "food",
            "fruit"
        ],
        createdAt: "2022-02-13T14:21:12.625",
        updatedAt: "2022-02-13T14:21:12.625"
    },
    {
        id: 9,
        name: "Norwegian Salmon",
        description: "Smoked Salamon from Norway",
        price: 5.79,
        categories: [
            "food",
            "fish"
        ],
        createdAt: "2022-02-13T14:21:12.627",
        updatedAt: "2022-02-13T14:21:12.627"
    },
    {
        id: 10,
        name: "Fresh Chicken",
        description: "Chicken imported from diffrent countries",
        price: 1.99,
        categories: [
            "food",
            "meats"
        ],
        createdAt: "2022-02-13T14:21:12.628",
        updatedAt: "2022-02-13T14:21:12.628"
    },
    {
        id: 11,
        name: "Beef Steak",
        description: "Beef Steak imported from England",
        price: 10.99,
        categories: [
            "food",
            "meats"
        ],
        createdAt: "2022-02-13T14:21:12.629",
        updatedAt: "2022-02-13T14:21:12.629"
    },
    {
        id: 12,
        name: "Strawberies",
        description: "Strawberies from Germany",
        price: 2.59,
        categories: [
            "food",
            "fruit"
        ],
        createdAt: "2022-02-13T14:21:12.631",
        updatedAt: "2022-02-13T14:21:12.631"
    },
    {
        id: 13,
        name: "Milk",
        description: "Aplen Milk",
        price: 0.79,
        categories: [
            "milk",
            "food"
        ],
        createdAt: "2022-02-13T14:21:12.632",
        updatedAt: "2022-02-13T14:21:12.632"
    },
    {
        id: 14,
        name: "Carrots",
        description: "Mama carrots",
        price: 0.59,
        categories: [
            "food",
            "legumes"
        ],
        createdAt: "2022-02-13T14:21:12.633",
        updatedAt: "2022-02-13T14:21:12.633"
    },
    {
        id: 15,
        name: "A4 Papers",
        description: "A4 Papers - Good Quality",
        price: 2.79,
        categories: [
            "office"
        ],
        createdAt: "2022-02-13T14:21:12.635",
        updatedAt: "2022-02-13T14:21:12.635"
    },
    {
        id: 16,
        name: "Aluminum foil",
        description: "Aluminum foil for food wrapping",
        price: 0.99,
        categories: [
            "kitchen"
        ],
        createdAt: "2022-02-13T14:21:12.636",
        updatedAt: "2022-02-13T14:21:12.636"
    },
    {
        id: 17,
        name: "Fresh foil",
        description: "Fresh foil for food wrapping",
        price: 0.79,
        categories: [
            "kitchen"
        ],
        createdAt: "2022-02-13T14:21:12.638",
        updatedAt: "2022-02-13T14:21:12.638"
    },
    {
        id: 18,
        name: "Avocado",
        description: "Good quality avocado from Ecuador",
        price: 1.29,
        categories: [
            "food",
            "fruit"
        ],
        createdAt: "2022-02-13T14:21:12.639",
        updatedAt: "2022-02-13T14:21:12.639"
    },,
    {
        id: 19,
        name: "Pen",
        description: "Good Pencil",
        price: 1.09,
        categories: [
            "office"
        ],
        createdAt: "2022-02-13T14:21:12.639",
        updatedAt: "2022-02-13T14:21:12.639"
    }
];